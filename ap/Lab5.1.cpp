// Lab5.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>


int main()
{
	int A[3][3] = { {1, 2, 3}, {4, 5, 6}, {7, 8, 9} };
	int B[3][3] = { {1, 1, 2}, {3, 5, 8}, {7, 9, 9} };
	int C[3][3];

	std::cout << "Massive A: " << std::endl;

	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			std::cout << A[i][j];
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;

	std::cout << "Massive B: " << std::endl;

	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			std::cout << B[i][j];
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;

	std::cout << "Massive C: " << std::endl;

	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			if (A[i][j] <= B[i][j])
			{
				C[i][j] = 1;
			}
			else
			{
				C[i][j] = 0;
			}
			std::cout << C[i][j];

		}
		std::cout << std::endl;
	}

    return 0;
}

