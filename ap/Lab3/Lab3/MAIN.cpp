// Lab3.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include "MAIN.h"

int main()
{
	char progaChoice;
	std::cout << "If You want to use the calculator - print \"c\", and if You want to determine the number of "
		"decimal digits in the entered number - print \"n\"" << std::endl;
	std::cin >> progaChoice;
	choice(progaChoice);
	return 0;
}

void choice(char a)
{
	if (a == 'c')
	{
		proga1();
	}
	else if (a == 'n')
	{
		proga2();
	}
}

void proga1()
{
	do
	{
		double z = 0;
		double x = 0;
		double y = 0;

		std::cout << "Print the value of \"z\"" << std::endl;
		std::cin >> z;
		x = fz(z);
		y = fx(x);
		std::cout << y << std::endl;
		std::cout << "If You want to continue, print \"y\"" << std::endl;
		std::cin >> a;
	} while (a == 'y');
}

double fz(double z)
{
	double result = 0;
	if (z < 0)
	{
		result = fz1(z);
	}
	else if (0 <= z && z <= 8)
	{
		result = fz2(z);
	}
	else if (z > 8)
	{
		result = fz3(z);
	}
	return result;
}

double fz1(double z)
{
	double result = 0;
	result = 2 * z - log(abs(z));
	return result;
}

double fz2(double z)
{
	double result = 0;
	result = tan(z) - 2 * z;
	return result;
}

double fz3(double z)
{
	double result = 0;
	result = pow(sin(z), 2);
	return result;
}

double fx(double x)
{
	double result2 = 0;
	double a = 0;

	if (x >= 0)
	{
		result2 = pow(x, (0.25)) + sin(x);
	}
	else
	{
		a = pow(x, 2);
		result2 = pow(a, (0.125)) + sin(x);
	}
	return result2;

}

void proga2()
{
	do
	{
		int enteredNum = 0;
		int answer = 0;
		int error = 0;

		std::cout << "Print any number in the range from 1 to 200" << std::endl;
		std::cin >> enteredNum;
		error = choice2(enteredNum);
		if (error == 0)
		{
			answer = decimalPlace(enteredNum);
			std::cout << "The number of decimal digits in " << enteredNum << " is " << answer << std::endl;
		}		
		std::cout << "If You want to continue, print \"y\"" << std::endl;
		std::cin >> a;
	} while (a == 'y');
}

int choice2(int number)
{
	if (number >= 1 && number <= 200)
	{
		number = decimalPlace(number);
		return 0;
	}
	else
	{
		std::cout << "The number must be in the range from 1 to 200" << std::endl;
		return 1;
	}
}

int decimalPlace(int number)
{
	int i = 0;
	while (number >= 1)
	{
		number = number / 10;
		i++;
		
	}
	
	return i;
}
