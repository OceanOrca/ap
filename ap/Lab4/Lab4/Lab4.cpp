// Lab4.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>


char chooseOperator;
double function1(double);
double function2(double);


int main()
{
	std::cout << "Choose which operator would you like to use. Print \'f\' to use \"for\" or \'w\' to use \"while\"" << std::endl;
	std::cin >> chooseOperator;

	if (chooseOperator == 'f')
	{
		for (double i = 0.6; i < 4.2; i = i + 0.3)
		{
			std::cout << i << std::endl;
			std::cout << function1(i) + function2(i) << std::endl;
		}
		system("pause");
	}
	else if (chooseOperator == 'w')
	{
		double x = 0.6;
		while (x <= 4.2)
		{
			std::cout << x << std::endl;
			std::cout << function1(x) + function2(x) << std::endl;
			x = x + 0.3;
		}
		system("pause");
	}
	else
	{
		std::cout << "Wrong choice. The program will be closed. You can restart it and try again" << std::endl;
		system("pause");
	}

	return 0;
}

double function1(double x)
{
	double result;
	result = exp(-x) + 4 * x;
	return result;
}

double function2(double x)
{
	double result;
	result = sqrt(1 + pow(x, 3) + 4 * (pow(x, 2)));
	return result;
}
